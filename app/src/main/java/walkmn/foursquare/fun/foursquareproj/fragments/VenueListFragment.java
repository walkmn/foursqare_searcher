package walkmn.foursquare.fun.foursquareproj.fragments;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import walkmn.foursquare.fun.foursquareproj.MainActivity;
import walkmn.foursquare.fun.foursquareproj.R;
import walkmn.foursquare.fun.foursquareproj.VenueAdapter;
import walkmn.foursquare.fun.foursquareproj.models.VenueModel;

public class VenueListFragment extends BaseFragment {

    private RecyclerView rvVenue;
    private VenueAdapter venueAdapter;
    private LinearLayoutManager mLayoutManager;

    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;
    private ArrayList<VenueModel> venueList = new ArrayList<>();

    public void setVenueItemListener(VenueItemListener venueItemListener) {
        this.venueItemListener = venueItemListener;
    }

    private VenueItemListener venueItemListener;

    public void loadVenueList(ArrayList<VenueModel> venueList) {
        int oldSize =  this.venueList.size();
        this.venueList.clear();
        this.venueList.addAll(venueList);
        int newSize =  this.venueList.size();

        if (oldSize == newSize || newSize < MainActivity.INCREASE_PLACE_COUNT_VALUE) {
            venueAdapter.disableLoading();
        }

        venueAdapter.notifyDataSetChanged();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_venue_list;
    }

    @Override
    protected void onInitView(View rootView) {
        rvVenue = (RecyclerView) rootView.findViewById(R.id.rv_venue);

        mLayoutManager = new LinearLayoutManager(mActivity);
        rvVenue.setLayoutManager(mLayoutManager);
        venueAdapter = new VenueAdapter(venueList, mActivity, venueItemListener);
        rvVenue.setAdapter(venueAdapter);

        rvVenue.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {

                            if (venueItemListener != null) {
                                venueItemListener.onLastItemShowed();
                            }
                        }
                    }
                }
            }
        });
    }

    public interface VenueItemListener {
        void onClickVenue(VenueModel venueModel);
        void onLastItemShowed();
    }
}
