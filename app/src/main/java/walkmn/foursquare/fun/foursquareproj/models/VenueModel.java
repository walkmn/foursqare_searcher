package walkmn.foursquare.fun.foursquareproj.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 08.03.2016.
 */
public class VenueModel implements Parcelable {

    private String id;
    private String name;
    private LocationModel location;

    public String getName() {
        return name;
    }

    public LocationModel getLocation() {
        return location;
    }

    protected VenueModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        location = (LocationModel) in.readValue(LocationModel.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeValue(location);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<VenueModel> CREATOR = new Parcelable.Creator<VenueModel>() {
        @Override
        public VenueModel createFromParcel(Parcel in) {
            return new VenueModel(in);
        }

        @Override
        public VenueModel[] newArray(int size) {
            return new VenueModel[size];
        }
    };
}