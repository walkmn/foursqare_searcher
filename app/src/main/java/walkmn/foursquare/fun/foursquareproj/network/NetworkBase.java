package walkmn.foursquare.fun.foursquareproj.network;


import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import walkmn.foursquare.fun.foursquareproj.network.models.NetworkResult;

/**
 * Created by user on 06.03.2016.
 */
public class NetworkBase {

    private static final String TAG = "Network";

    private static final int READ_TIME_OUT = 30000;

    private String uri;

    public NetworkBase(String uri) {
        this.uri = uri;
    }

    public NetworkResult execute() {

        Log.d(TAG, "request: " + uri);

        NetworkResult networkResult = new NetworkResult();

        URL url;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL(uri);

            urlConnection = (HttpURLConnection) url
                    .openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setReadTimeout(READ_TIME_OUT);
            urlConnection.connect();

            networkResult.setCode(urlConnection.getResponseCode());

            InputStream in = urlConnection.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder buf = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                buf.append(line + "\n");
            }
            networkResult.setJson(buf.toString());

            Log.d(TAG, "response code: " + networkResult.getCode());
            Log.d(TAG, "response json: " + networkResult.getJson());

            return networkResult;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return networkResult;
    }
}
