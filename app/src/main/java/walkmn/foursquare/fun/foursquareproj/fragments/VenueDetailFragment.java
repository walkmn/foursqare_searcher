package walkmn.foursquare.fun.foursquareproj.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import walkmn.foursquare.fun.foursquareproj.R;
import walkmn.foursquare.fun.foursquareproj.VenueAdapter;
import walkmn.foursquare.fun.foursquareproj.models.VenueModel;

public class VenueDetailFragment extends BaseFragment {

    public final static String PARCELABLE_VENUE_OBJ = "PARCELABLE_VENUE";

    private VenueModel venue;

    TextView tv_name;
    TextView tv_country;
    TextView tv_state;
    TextView tv_city;
    TextView tv_cross_street;
    TextView tv_distance;

    Button bt_back;
    Button bt_open_in_map;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_venue_details;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected void onInitView(View rootView) {

        if (getArguments() == null || getArguments().getParcelable(PARCELABLE_VENUE_OBJ) == null) {
            throw new NullPointerException("You must pass parcelable object `venue` to this fragment.");
        }

        venue = getArguments().getParcelable(PARCELABLE_VENUE_OBJ);

        tv_name = (TextView) rootView.findViewById(R.id.tv_name);
        tv_country = (TextView) rootView.findViewById(R.id.tv_country);
        tv_state = (TextView) rootView.findViewById(R.id.tv_state);
        tv_city = (TextView) rootView.findViewById(R.id.tv_city);
        tv_cross_street = (TextView) rootView.findViewById(R.id.tv_cross_street);
        tv_distance = (TextView) rootView.findViewById(R.id.tv_distance);
        bt_back = (Button) rootView.findViewById(R.id.bt_back);
        bt_open_in_map = (Button) rootView.findViewById(R.id.bt_open_in_map);

        if (venue.getLocation().getState() == null) {
            tv_state.setVisibility(View.GONE);
        }
        if (venue.getLocation().getCity() == null) {
            tv_city.setVisibility(View.GONE);
        }
        if (venue.getLocation().getCrossStreet() == null) {
            tv_cross_street.setVisibility(View.GONE);
        }

        tv_name.setText(venue.getName());
        tv_country.setText(getString(R.string.country, venue.getLocation().getCountry()));
        tv_state.setText(getString(R.string.state, venue.getLocation().getState()));
        tv_city.setText(getString(R.string.city, venue.getLocation().getCity()));
        tv_cross_street.setText(getString(R.string.street, venue.getLocation().getCrossStreet()));
        tv_distance.setText(getString(R.string.distance, venue.getLocation().getDistance()));


        bt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });
        bt_open_in_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent initent = new Intent(Intent.ACTION_VIEW,Uri.parse("geo:" + venue.getLocation().getLat() + "," + venue.getLocation().getLng()));
                initent.setClassName("com.google.android.apps.maps",
                        "com.google.android.maps.MapsActivity");
                startActivity(initent);
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.action_settings_current).setVisible(false);
        menu.findItem(R.id.action_settings_new_york).setVisible(false);
    }
}
