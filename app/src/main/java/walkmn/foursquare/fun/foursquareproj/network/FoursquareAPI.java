package walkmn.foursquare.fun.foursquareproj.network;

import android.app.Activity;

import walkmn.foursquare.fun.foursquareproj.network.models.VenuesCriteria;

/**
 * Created by user on 06.03.2016.
 */
public class FoursquareAPI implements APIConst {

    private Activity mActivity;

    public FoursquareAPI(Activity activity) {
        this.mActivity = activity;
    }

    public void getVenuesNearby(VenuesCriteria criteria, NetworkResultListener networkResultListener, boolean enableProgressDialog) {

        String params =  SEARCH + "?v=" + API_DATE_VERSION
                + "&ll="
                + criteria.getLocation().getLatitude()
                + ","
                + criteria.getLocation().getLongitude()
                + "&llAcc="
                + criteria.getLocation().getAccuracy()
                + "&query="
                + criteria.getQuery()
                + "&limit="
                + criteria.getQuantity()
                + "&intent="
                + criteria.getIntent().getValue()
                + "&radius="
                + criteria.getRadius()
                + "&client_id=" + CLIENT_ID + "&client_secret=" + CLIENT_SECRET;

        new NetworkExecutor(params, mActivity)
                .setNetworkResultListener(networkResultListener)
                .enableProgressDialog(enableProgressDialog)
                .enableCache()
                .execute();
    };
}
