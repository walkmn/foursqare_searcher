package walkmn.foursquare.fun.foursquareproj;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.ButterKnife;
import walkmn.foursquare.fun.foursquareproj.fragments.VenueDetailFragment;
import walkmn.foursquare.fun.foursquareproj.fragments.VenueListFragment;
import walkmn.foursquare.fun.foursquareproj.models.VenueModel;
import walkmn.foursquare.fun.foursquareproj.network.FoursquareAPI;
import walkmn.foursquare.fun.foursquareproj.network.NetworkResultListener;
import walkmn.foursquare.fun.foursquareproj.network.models.VenuesCriteria;

public class MainActivity extends LocationFinderActivity implements VenueListFragment.VenueItemListener {

    public static final int INCREASE_PLACE_COUNT_VALUE = 10;

    private ArrayList<VenueModel> venuesList = new ArrayList<>();
    private boolean isUseNewYorkLocation = false;

    private VenueListFragment venueListFragment;

    private int currentPlaceCount = INCREASE_PLACE_COUNT_VALUE;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        venueListFragment = new VenueListFragment();
        venueListFragment.setVenueItemListener(this);
        loadFragment(venueListFragment);

        makeRequest(currentPlaceCount, true);
    }

    private void makeRequest(int placeCount, boolean enableProgressDialog) {
        isLoading = true;

        Location currentLocation = getLocation(isUseNewYorkLocation);
        
        if (currentLocation == null) {
            Toast.makeText(MainActivity.this, "Please turn on GPS.", Toast.LENGTH_SHORT).show();
            return;
        }

        VenuesCriteria venuesCriteria = new VenuesCriteria();
        venuesCriteria.setQuantity(placeCount);
        venuesCriteria.setLocation(getLocation(isUseNewYorkLocation));
        venuesCriteria.setQuery("pizza");


        new FoursquareAPI(MainActivity.this).getVenuesNearby(venuesCriteria, new NetworkResultListener() {
            @Override
            public void onSuccess(String json) {
                isLoading = false;

                JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
                venuesList = new Gson().fromJson(jsonObject.getAsJsonObject("response").getAsJsonArray("venues"), new TypeToken<ArrayList<VenueModel>>() {}.getType());

                if (venuesList.size() == 0) {
                    Toast.makeText(MainActivity.this, "Pizza place is not found.", Toast.LENGTH_SHORT).show();
                }

                Collections.sort(venuesList, new VenueComparator());
                venueListFragment.loadVenueList(venuesList);
            }

            @Override
            public void onError(String errorMessage) {
                isLoading = false;
                Toast.makeText(MainActivity.this, "Sever error: " + errorMessage, Toast.LENGTH_SHORT).show();
            }
        }, enableProgressDialog);
    }

    private void loadFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
                .add(R.id.container, fragment)
                .addToBackStack(fragment.getTag())
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        if (isUseNewYorkLocation) {
            menu.findItem(R.id.action_settings_current).setVisible(true);
            menu.findItem(R.id.action_settings_new_york).setVisible(false);
        } else {
            menu.findItem(R.id.action_settings_current).setVisible(false);
            menu.findItem(R.id.action_settings_new_york).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings_new_york) {
            isUseNewYorkLocation = true;
            invalidateOptionsMenu();
            makeRequest(currentPlaceCount, true);
            return true;
        }

        if (id == R.id.action_settings_current) {
            isUseNewYorkLocation = false;
            invalidateOptionsMenu();
            makeRequest(currentPlaceCount, true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickVenue(VenueModel venueModel) {
        VenueDetailFragment venueDetailFragment = new VenueDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(VenueDetailFragment.PARCELABLE_VENUE_OBJ, venueModel);
        venueDetailFragment.setArguments(bundle);

        loadFragment(venueDetailFragment);
    }

    @Override
    public void onLastItemShowed() {
        if (!isLoading) {
            makeRequest(currentPlaceCount += INCREASE_PLACE_COUNT_VALUE, false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getSupportFragmentManager().getBackStackEntryCount() < 1) {
            finish();
        }
    }

    public class VenueComparator implements Comparator<VenueModel> {
        @Override
        public int compare(VenueModel o1, VenueModel o2) {
            if (o1.getLocation().getDistance() < o2.getLocation().getDistance()) {
                return -1;
            }
            return 1;
        }
    }
}
