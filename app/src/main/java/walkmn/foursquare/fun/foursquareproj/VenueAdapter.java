package walkmn.foursquare.fun.foursquareproj;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import walkmn.foursquare.fun.foursquareproj.fragments.VenueListFragment;
import walkmn.foursquare.fun.foursquareproj.models.VenueModel;

/**
 * Created by user on 08.03.2016.
 */
public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.VenueHolder> {

    private ArrayList<VenueModel> venueList = new ArrayList<>();
    private Context mContext;
    private VenueListFragment.VenueItemListener venueItemListener;
    private boolean disableLoading = false;

    public VenueAdapter(ArrayList<VenueModel> venueList, Context mContext, VenueListFragment.VenueItemListener venueItemListener) {
        this.venueList = venueList;
        this.mContext = mContext;
        this.venueItemListener = venueItemListener;
    }


    @Override
    public VenueHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_venue, parent, false);
        return new VenueHolder(view);
    }

    @Override
    public void onBindViewHolder(VenueHolder holder, int position) {

        final VenueModel venueModel = venueList.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (venueItemListener != null) venueItemListener.onClickVenue(venueModel);
            }
        });
        holder.tvName.setText(venueModel.getName());
        holder.tvDistance.setText(mContext.getString(R.string.distance, venueModel.getLocation().getDistance()));

        if (position == venueList.size() - 1 && !disableLoading) {
            holder.progressBar.setVisibility(View.VISIBLE);
        } else {
            holder.progressBar.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return venueList.size();
    }

    public void disableLoading() {
        disableLoading = true;
    }

    public static class VenueHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvDistance;
        ProgressBar progressBar;

        public VenueHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvDistance = (TextView) itemView.findViewById(R.id.tv_distance);

            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }
}
