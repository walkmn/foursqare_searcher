package walkmn.foursquare.fun.foursquareproj.network;

/**
 * Created by walkmn on 23.12.15.
 */
public interface NetworkResultListener {
    void onSuccess(String json);
    void onError(String errorMessage);
}
