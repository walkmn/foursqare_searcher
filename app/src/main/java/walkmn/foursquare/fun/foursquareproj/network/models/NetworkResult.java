package walkmn.foursquare.fun.foursquareproj.network.models;

/**
 * Created by user on 07.03.2016.
 */
public class NetworkResult {

    private int code;
    private String json;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getJson() {
        return json;
    }

    public boolean isSuccess() {
        return code == 200 && json != null;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
