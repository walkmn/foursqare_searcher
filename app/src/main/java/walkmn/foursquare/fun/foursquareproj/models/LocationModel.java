package walkmn.foursquare.fun.foursquareproj.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by user on 08.03.2016.
 */
public class LocationModel implements Parcelable {

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    private double lat;
    private double lng;
    private int distance;
    private int postalCode;
    private String crossStreet;
    private String cc;
    private String city;
    private String state;
    private String country;

    public int getDistance() {
        return distance;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public String getCrossStreet() {
        return crossStreet;
    }

    protected LocationModel(Parcel in) {
        lat = in.readDouble();
        lng = in.readDouble();
        distance = in.readInt();
        postalCode = in.readInt();
        crossStreet = in.readString();
        cc = in.readString();
        city = in.readString();
        state = in.readString();
        country = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(lat);
        dest.writeDouble(lng);
        dest.writeInt(distance);
        dest.writeInt(postalCode);
        dest.writeString(crossStreet);
        dest.writeString(cc);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(country);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<LocationModel> CREATOR = new Parcelable.Creator<LocationModel>() {
        @Override
        public LocationModel createFromParcel(Parcel in) {
            return new LocationModel(in);
        }

        @Override
        public LocationModel[] newArray(int size) {
            return new LocationModel[size];
        }
    };
}
