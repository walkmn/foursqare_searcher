package walkmn.foursquare.fun.foursquareproj.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import walkmn.foursquare.fun.foursquareproj.network.models.NetworkResult;

/**
 * Created by user on 07.03.2016.
 */
public class NetworkExecutor implements APIConst {

    private static final String CACHE = "CACHE";

    private String urlParams;

    private boolean enabledCache = false;
    private NetworkResultListener networkResultListener;
    private ProgressDialog mProgress;
    private android.content.Context mActivity;
    private boolean enableProgressDialog;

    public NetworkExecutor(String urlParams, Context mActivity) {
        this.mActivity = mActivity;
        this.urlParams = HOST_FULL + urlParams;
    }

    public NetworkExecutor enableCache() {
        this.enabledCache = true;
        return this;
    }

    public NetworkExecutor enableProgressDialog(boolean enableProgressDialog) {
        this.enableProgressDialog = enableProgressDialog;
        return this;
    }

    public void execute() {

        new AsyncTask<Void, Void, NetworkResult>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                if (enableProgressDialog) {
                    mProgress = new ProgressDialog(mActivity);
                    mProgress.setCancelable(false);
                    mProgress.setMessage("Loading...");
                    mProgress.show();
                }
            }

            @Override
            protected NetworkResult doInBackground(Void... params) {
                return new NetworkBase(urlParams).execute();
            }

            @Override
            protected void onPostExecute(NetworkResult result) {
                super.onPostExecute(result);

                if (mProgress != null) mProgress.dismiss();

                if (networkResultListener != null) {

                    if (result.isSuccess()) {

                        if (enabledCache) {
                            saveToCache(urlParams.hashCode(), result.getJson());
                        }

                        networkResultListener.onSuccess(result.getJson());
                    } else {

                        // try to restore

                        if (enabledCache && isExistInCache(urlParams.hashCode())) {
                            networkResultListener.onSuccess(restoreFromCache(urlParams.hashCode()));
                        } else {
                            networkResultListener.onError(result.getJson());
                        }
                    }
                }
            }
        }.execute();
    }

    public NetworkExecutor setNetworkResultListener(NetworkResultListener networkResultListener) {
        this.networkResultListener = networkResultListener;
        return this;
    }

    // helper (for persist)
    private boolean isExistInCache(int hashCode) {
        SharedPreferences sp = mActivity.getSharedPreferences(CACHE, Context.MODE_PRIVATE);
        return sp.contains(String.valueOf(hashCode));
    }

    private void saveToCache(int hashCode, String json) {
        SharedPreferences sp = mActivity.getSharedPreferences(CACHE, Context.MODE_PRIVATE);
        sp.edit()
                .putString(String.valueOf(hashCode), json)
                .apply();
    }

    private String restoreFromCache(int hashCode) {
        SharedPreferences sp = mActivity.getSharedPreferences(CACHE, Context.MODE_PRIVATE);
        return sp.getString(String.valueOf(hashCode), null);
    }

}
